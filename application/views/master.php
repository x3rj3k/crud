<!DOCTYPE html>
<html  >

<head>
 
   <title >Codeigniter MySQL MySQL</title>

 
 <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

    
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo site_url('assets/css/style.css'); ?>">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
 
    <body>
     
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header"> 
                    <a class="navbar-brand" href="#">GROUP 6 CRUD</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>
                    </ul>

                </div>
            </div>
        </nav>


<div class="container " > 
    <div class="row content">
<?php
if(isset($content)){
    echo $content;
}
?>
    
     </div>
    
</div>
    
  
</body>
</html>